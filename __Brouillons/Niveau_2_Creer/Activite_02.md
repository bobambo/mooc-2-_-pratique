# Langage SQL

## Contexte

Vous enseignez la NSI à un groupe de 34 élèves en terminale, vos 6 heures se répartissent
comme suit : 4h en effectif complet (2h + 2h) et 2h en effectif réduit (17 élèves par groupe),
vous avez, pendant ces 2h, une salle de 24 ordinateurs sous GNU/Linux à votre disposition.
Vous désirez préparer une ou plusieurs séances consacrées à la partie “Langage SQL :
requêtes d’interrogation et de mise à jour d’une base de données.” (voir l'extrait du BO ci-dessous)

## A réaliser

Proposer votre première séance sur le sujet. Précisez notamment les objectifs, le matériel, les pré-requis.