Dans cette première séquence du _Bloc 2. Mise en pratique_ nous vous proposons de vous entraîner à :

1. **proposer un contenu de cours adapté aux élèves**. Ce contenu pourra être :
    - recherché et adapté dans une banque de ressources 
    - ou construit entièrement.
2. **créer la fiche _prof_** correspondant à la fiche élève.

Nous voilà donc en train d'apprendre en faisant : on commence par la pratique, on prendra du recul dans un deuxième temps.

## Objectifs
Les objectifs visés sont :

- À partir d'une activité existante
    - Déterminer les objectifs (connaissances et compétences) d'une activité
    - Identifier les prérequis d'une activité
    - Juger si une activité est appropriée ou pas
    - Adapter cette activité
  
- **Créer votre propre activité**
    - Construire une activité élève (en s'inspirant d'une activité existante ou à partir d'une idée originale) ; il pourra s'agir d'une activité de compréhension en utilisant des exercices appropriés au concept ou bien d'une activité de production.
  
- **Créer une _fiche professeur_**

## Boîte à outils

Pour vous préparer, vous aurez à votre disposition : 

- Les éléments constitutifs d'une fiche prof _(ci-dessous)_
- La deuxième partie, du livre "Enseigner l'Informatique" de Werner Hartmann
- Des ressources de collègues pour :
    - Comprendre les intentions d'une activité avec l'activité Turtle
    - Toucher du doigt un exemple concret de fiche élève et de fiche prof avec l'activité Bases de Données
    - Découvrir un puits de ressources pédagogiques avec le concours castor informatique
- La _to do_ liste de fin, avec les actions à réaliser pour répondre aux objectifs ET pour bien se préparer pour l'évaluation par les pairs.

Et bien sûr la [communauté d’apprentissage et de pratique grâce au forum](https://mooc-forums.inria.fr/moocnsi/c/mooc-2/penser-concevoir-elaborer/192) qui fait converger le forum de la communauté CAI (Communauté d'Apprentissage de l'Informatique) et les forums des Moocs NSI.


## La fiche _prof_ d'une activité : choisir les métas-informations

Voici les méta-informations qu'il est courant de renseigner, cela nous aide à gérer nos ressources personnelles, et à les partager, les faire évoluer à partir d'autres, etc. Cela permet surtout de bien expliciter sa démarche pédagogique. Ce format est inspiré à la fois des ressources de sites comme la [Main à la pâte](https://www.fondation-lamap.org/) et des standards officiels comme la [LOMfr](https://fr.wikipedia.org/wiki/Learning_Object_Metadata)…)

**Thématique :** Quel est le thème principal de l'activité ? On pourra ici donner un lien vers le programme officiel par exemple.

**Notions liées :** Seront précisées ici les notions directement rattachées à l'activité ou les sous-thèmes en relation avec le sujet principal.

**Résumé de l’activité :** Une description de ce que propose l'activité ; s'il s'agit d'une suite d'exercices ou d'une activité débranchée, s'il s'agit de programmer en utilisant un module particulier de Python etc.

**Objectifs :** Pourquoi propose-t-on cette activité ? Quelle acquisition de connaissance est visée ? Quelle manipulation favorise le développement d'une compétence ?

**Auteur :**

**Durée de l’activité :**

**Forme de participation :** individuelle ? en binôme ? en groupe ? en autonomie ?

**Matériel nécessaire :**

**Préparation :**

**Autres références :** liens éventuels vers d'autres cours ou activités similaires ou complémentaires

Une fiche _prof_ vierge est disponible ici : [Fiche prof vierge](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche_prof_vierge.md)
