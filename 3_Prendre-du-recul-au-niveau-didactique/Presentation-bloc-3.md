# Didactique, pédagogie, épistémologie

Dans ce troisième bloc, des interviews, des vidéos, vont nous permettre une prise de recul sur nos productions et notre activité d'enseigner. En trois modules, nous explorons le monde de la didactique, de l'épistémologie et la pédagogie de l'égalité.

- Olivier, Béatrice, Cédric sont chercheur.es en didactique et nous proposent quelques exemples de questionnements qui alimentent leurs recherches ;
- Violaine Prince, Professeure d'Informatique, nous parle d'épistémologie et du lien avec les autres disciplines ;
- Quelques références aux travaux d'Isabelle Collet nous met en garde contre les biais inégalitaires dans nos enseignements.

Le visionnage de ces ressources multimédia pourra se prolonger par des échanges sur le forum NSI et sera validé par une série de questions à la fin de chaque séquence.
